var GameLayer = cc.LayerColor.extend({
    numOfBall:0,
    ballStatus:2,
    max:5,
    min:0,
    x:null,
    hitFlag:0,
    numOfMessage:0,



    ctor: function() {
        this._super( new cc.Color4B( 0, 0, 0, 0 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.maze = new Maze(0,0);
        this.addChild( this.maze );


        //this.maze.scheduleUpdate();
        this.jumper = new Jumper( 1300, 900 ,this);
        this.x = cc.Follow.create( this.jumper, cc.rect( 0, 0, 1600, 1040) );
        this.runAction(this.x);

        this.scheduleUpdate();

        
        //this.jumper.setPositionZ(1);
        this.jumper.setBlocks( this.maze.blocks );
        this.addChild( this.jumper );
        this.scheduleOnce(function() {
            this.jumper.scheduleUpdate();
        }, 2);

        this.flagGoal = this.maze.getFlagGoal();
        

        this.maze.setPositionByBall(this.jumper);

        //this.maze.followBallAction();
        
        
        
        this.setKeyboardEnabled( true );
        
        return true;
    },

    changeBallStatus: function(e){
        var DetailLayer = this.getParent().getChildByTag("DetailLayer");

        if(e==81){
            if(this.ballStatus >this.min){
                 this.ballStatus--;
                 DetailLayer.updateText(this.ballStatus);
                 console.log('ballStatus' + this.ballStatus);
            }
        }
        else if(e==87){
            if(this.ballStatus <this.max){
                this.ballStatus++;
                DetailLayer.updateText(this.ballStatus);
                console.log('ballStatus' + this.ballStatus);
            }
        }
    },

    changeBall: function(){
        console.log('this.numOfBall'+this.numOfBall);
        if(this.numOfBall == 1){
            this.ball = new Ball1(this.jumper.getPositionX(),this.jumper.getPositionY());
            this.jumper.unscheduleUpdate();
            this.removeChild(this.jumper);
            
            this.numOfBall = 0;

            this.jumper = this.ball;
            this.jumper.setBlocks( this.maze.blocks );
            this.addChild(this.jumper);
            this.jumper.scheduleUpdate();
            this.stopAllActions();

            this.x = cc.Follow.create( this.jumper, cc.rect( 0, 0, 1600, 1040) );
            this.runAction(this.x);
        }
        else if(this.numOfBall == 2){
            this.ball = new Jumper(this.jumper.getPositionX(),this.jumper.getPositionY());
            this.jumper.unscheduleUpdate();
            this.removeChild(this.jumper);
            
            this.numOfBall = 0;

            this.jumper = this.ball;
            this.jumper.setBlocks( this.maze.blocks );
            this.addChild(this.jumper);
            this.jumper.scheduleUpdate();
            this.stopAllActions();

            this.x = cc.Follow.create( this.jumper, cc.rect( 0, 0, 1600, 1040) );
            this.runAction(this.x);

        }
        
        
    


    },

    update: function(){
        
        if(this.flagGoal.toGoal(this.jumper)) {
            if(this.hitFlag == 0 && !this.moveAction){
                this.moveAction = cc.MoveTo.create(1,cc.p( this.flagGoal.getPosition().x , this.flagGoal.getPosition().y+320)); 
                this.flagGoal.runAction(this.moveAction);
                this.hitFlag++;
                //var DetailLayer = this.getParent().getChildByTag("DetailLayer");
                // DetailLayer.updateClearStage();
            }
            else if(this.hitFlag == 1 && this.moveAction.isDone()) {
                this.moveAction = cc.MoveTo.create(1,cc.p( this.flagGoal.getPosition().x , this.flagGoal.getPosition().y+320)); 
                this.flagGoal.runAction(this.moveAction);
                this.hitFlag++;

            }
            else if(this.hitFlag == 2 && this.moveAction.isDone()){
                var DetailLayer = this.getParent().getChildByTag("DetailLayer");
                DetailLayer.updateClearStage();
                this.hitFlag++;

                this.runAction(cc.Sequence.create(
                    cc.DelayTime.create(7),
                    cc.CallFunc.create(function(node) {
                        var scene = new StartScene2();
                        var gameTransition = cc.TransitionFade.create(1, scene);
                        cc.Director.getInstance().replaceScene(gameTransition);
                        
                    }, this)
                ));
            }
            




        }
        console.log(this.jumper.getPositionX() +" " + this.jumper.getPositionY());

            if(this.jumper.getPositionX() >= 1360&&this.jumper.getPositionX() <= 1440){
                if(this.jumper.getPositionY()==537){
                    this.numOfMessage++;
                    var DetailLayer = this.getParent().getChildByTag("DetailLayer");
                    var audioEngine = cc.AudioEngine.getInstance();
                    if(this.numOfMessage %2== 1){
                        console.log('music');
                        // audioEngine.playMusic(s_msnSound, true);
                        // cc.
                        // audioEngine.stopMusic(s_msnSound);
                        DetailLayer.addText3();
                    }
                    else{
                        DetailLayer.removeText3();
                    }
                }
            }
        //console.log(this.jumper.y);
    },

    Jumper: function() {
        return this.jumper; 
    },

    

    onKeyDown: function( e ) {
        console.log(e);
        if (e == cc.KEY.r)
            location.reload();
        this.jumper.handleKeyDown( e );
        
        this.changeBallStatus(e);
        if(e == 32){
            this.numOfBall = this.ballStatus;
            this.changeBall();
            
        }

        
        
    },

    onKeyUp: function( e ) {
        this.jumper.handleKeyUp( e );
        //this.ball.handleKeyUp(e);
    }
});

var StartScene = cc.Scene.extend({
    ctor: function() {
        this._super(); 

        this.addChild( new GameLayer(),0,"GameLayer");
        var Game = this.getChildByTag("GameLayer");
        this.addChild( new DetailLayer(Game.ballStatus),100,"DetailLayer");
        this.addChild( new Background(),-100,"Background");
    }
});

// GameLayer.create = function() {

//     var layer = new GameLayer();
//     layer.init();
//     return layer;
// };


// GameLayer.scene = function () {
//     var scene = cc.Scene.create();
//     var layer = GameLayer.create();
//     var background = new Background();
//     var detailLayer = new DetailLayer();

//     background.init(layer.jumper);
//     detailLayer.init(layer.ballStatus);

//     scene.addChild(background,-100);
//     scene.addChild(layer);
//     scene.addChild(detailLayer);
//     return scene;

// };
