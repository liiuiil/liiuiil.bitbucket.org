var MenuLayer = cc.Layer.extend({
    trollSprite: null,
    troll:[],
    stack:0,

    ctor : function(){
        this._super();

    },
    init:function(){
        this._super();

        this.setTouchEnabled(true);
        this.setTouchMode(1);

        var director = cc.Director.getInstance();
        var winsize = director.getWinSize();
        var centerpos = cc.p(winsize.width / 2, winsize.height / 2);

        var bg = cc.Sprite.create(s_backgroundMenu);
        bg.setPosition(centerpos);

        this.addChild(bg);

        
        var logo = cc.Sprite.create(s_label);
        logo.setPosition(cc.p(winsize.width / 2, 500));
        this.addChild(logo, 1);


        this.troll[0] = cc.Sprite.create(s_troll[0]);
        this.troll[0].setPosition(cc.p(670, 370));
        this.addChild(this.troll[0], 10);
        
        var textField = cc.Sprite.create(s_clickToStart);
        textField.setAnchorPoint( cc.p( 0.5, 0.5));
        textField.setPosition( cc.p( 300, 140));
        textField.setOpacity(0);
        

        var fadeIn = cc.FadeIn.create(1.0);
        var fadeOut = cc.FadeOut.create(1.0);
        var delay = cc.DelayTime.create(0.5);
        textField.runAction(cc.RepeatForever.create(cc.Sequence.create(fadeIn, delay, fadeOut)));
        this.addChild(textField,300);
    },
    onTouchBegan:function(touch, event) {
        this.onPlay();
    },

    onPlay : function(){
        this.stack++;

        this.removeChild(this.troll[0]);
        this.troll[1] = cc.Sprite.create(s_troll[1]);
        this.troll[1].setPosition(cc.p(670, 370));
        this.addChild(this.troll[1], 10);


        if(this.stack == 3) {
            cc.Director.getInstance().replaceScene(cc.TransitionFade.create(1.5, new StartScene()));
        }
    }
});
var menuScene = cc.Scene.extend({
     ctor:function () {
        this._super();
        var layer = new MenuLayer();
        layer.init();
        this.addChild(layer);
    }
});