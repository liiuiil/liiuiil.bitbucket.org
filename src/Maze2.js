var Maze2 = cc.Node.extend({

    blocks:[],
    ball:null,
    flagGoal:null,
    redBlock:[],
    soundBlock:[],

    ctor: function( x , y) {
        this.x = x;
        this.y = y;
        this._super();
        this.WIDTH = 20;
        this.HEIGHT = 13;
        this.MAP = [
            '####################',
            '#..................#',
            '#.###.###..###.###.#',
            '#.#...#......#...#.#',
            '#.#.###......###.#.#',
            '#.#.#..........#.#.#',
            '#.........##.......#',
            '#.......##FF##....S#',
            '#.........##.......#',
            '#..................#',
            '#...$$$...........$#',
            '#.##.............#.#',
            '####################'
        ];
 
        // ...  code for drawing the maze has be left out
        this.addBox();
    },
    setPositionByBall: function(aBall){
        ball = aBall;
        //posBall = ball.getPosition();
        
    },

    getRedBlock: function(){
    	return this.redBlock;
    },

    getSoundBlock: function(){
    	return this.soundBlock;
    },	

    getFlagGoal: function(){
        return this.flagGoal;
    },

  

    addBox: function(){
    for ( var r = 0; r < this.HEIGHT; r++ ) {
        for ( var c = 0; c < this.WIDTH; c++ ) {
            if ( this.MAP[ r ][ c ] == '#' ) {
                var s = new Block(0,0,80,80);
                s.number = (r*10)+c;
                s.setAnchorPoint( cc.p( 0, 0 ) );
                s.setPosition( cc.p( c * 80, (this.HEIGHT - r -1) * 80 ) );
                this.blocks.push(s);
                this.addChild( s );
            }
            else if(this.MAP[r][c] == 'F'){
                this.flagGoal = new FlagGoal(0,0,80,80);
                //var s = cc.Sprite.create('images/flagGoal.png');
                this.flagGoal.number = (r*10)+c;
                this.flagGoal.setAnchorPoint( cc.p( 0, 0 ) );
                this.flagGoal.setPosition( cc.p( c * 80, (this.HEIGHT - r -1) * 80 ) );
                this.addChild( this.flagGoal );

            }
            else if(this.MAP[r][c] == '$'){
                var ss = new RedBlock(0,0,80,80);
                ss.number = (r*10)+c;
                ss.setAnchorPoint( cc.p( 0, 0 ) );
                ss.setPosition( cc.p( c * 80, (this.HEIGHT - r -1) * 80 ) );
                this.blocks.push(ss);
                this.addChild( ss );
                this.redBlock.push(ss);
            }

            else if(this.MAP[r][c] == 'S'){
                var ss = new SoundBlock(0,0,80,80);
                ss.number = (r*10)+c;
                ss.setAnchorPoint( cc.p( 0, 0 ) );
                ss.setPosition( cc.p( c * 80, (this.HEIGHT - r -1) * 80 ) );
                this.blocks.push(ss);
                this.addChild( ss );
                this.soundBlock.push(ss);

            }
        }



        this.setAnchorPoint( cc.p( 0, 0 ) );
        //var followAction = cc.Follow.create(ball);
        //this.runAction(followAction);
    }
}



 
    
});