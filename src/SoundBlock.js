var SoundBlock = cc.Sprite.extend({
    ctor: function( x1, y1, x2, y2 ) {
        this._super();
        this.initWithFile( 'images/iconSound.png',
                           cc.rect( 0, 0, x2-x1, y2 - y1 ) );
        this.setAnchorPoint( cc.p( 0, 0 ) );
        this.setPosition( cc.p( x1, y1 ) );
        this.number = -1;
    },

    getTopY: function() {
        return cc.rectGetMaxY( this.getBoundingBox() );
    },

    hitTop: function( oldRect, newRect ) {
        var brect = this.getBoundingBoxToWorld();
        if ( cc.rectGetMinY( oldRect ) >= cc.rectGetMaxY( brect ) ) {
            var uRect = cc.rectUnion( oldRect, newRect );
            return cc.rectIntersectsRect( uRect, brect );
        }
        return false;
    },

    onTop: function( rect ) {
        var brect = this.getBoundingBoxToWorld();
        var bminx = cc.rectGetMinX( brect );
        var bmaxx = cc.rectGetMaxX( brect );
        var minx = cc.rectGetMinX( rect );
        var maxx = cc.rectGetMaxX( rect );
        return ( minx <= bmaxx ) && ( bminx <= maxx );
    },
    getBottomY: function() {
        return cc.rectGetMinY( this.getBoundingBoxToWorld() );
    },

    hitBottom: function(oldRect,newRect){
        var brect = this.getBoundingBoxToWorld();
        if(cc.rectGetMaxY(oldRect) <= cc.rectGetMinY(brect)){
            //var newBrect = cc.rect(cc.rectGetMinX(brect)-2,cc.rectGetMinY(brect),cc.rectGetMaxX(brect)-2,cc.rectGetMaxY(brect));
            var uRect = cc.rectUnion( oldRect, newRect );
            return cc.rectIntersectsRect( uRect, brect );

        }
        return false;
    },

    getLeftX: function() {
        return cc.rectGetMinX( this.getBoundingBoxToWorld() );
    },

    hitLeftSide: function(oldRect,newRect){
        var brect = this.getBoundingBoxToWorld();
        if(cc.rectGetMaxX(oldRect) <= cc.rectGetMinX(brect)&& 
            (cc.rectGetMinY(oldRect)<=cc.rectGetMinY(brect)||(cc.rectGetMaxY(newRect)<=cc.rectGetMaxY(brect))||(cc.rectGetMaxY(newRect)<cc.rectGetMinY(brect)))
            &&(cc.rectGetMaxY(newRect)>cc.rectGetMinY(brect))){
            var uRect = cc.rectUnion( oldRect, newRect );
            return cc.rectIntersectsRect( uRect, brect );

        }
        return false;
    },

    getRightX: function() {
        return cc.rectGetMaxX( this.getBoundingBoxToWorld() );
    },

    hitRightSide: function(oldRect,newRect){
        var brect = this.getBoundingBoxToWorld();
        if(cc.rectGetMinX(oldRect) >= cc.rectGetMaxX(brect)&& 
            (cc.rectGetMinY(oldRect)<=cc.rectGetMinY(brect)||(cc.rectGetMaxY(newRect)<=cc.rectGetMaxY(brect))||(cc.rectGetMaxY(newRect)<cc.rectGetMinY(brect)))
            &&(cc.rectGetMaxY(newRect)>cc.rectGetMinY(brect))){
            var uRect = cc.rectUnion( oldRect, newRect );
            return cc.rectIntersectsRect( uRect, brect );

        }
        return false;
    }


});