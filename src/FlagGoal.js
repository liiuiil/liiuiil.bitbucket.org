var FlagGoal = cc.Sprite.extend({
	ctor: function(x1,y1,x2,y2){
		this._super();
		this.initWithFile('images/flagGoal.png' ,cc.rect( 0, 0, x2-x1, y2 - y1 ) );
		this.setAnchorPoint(0,0);
		this.setPosition( cc.p( x1, y1 ) );

	},

	toGoal: function(jumper){
		var rect = jumper.getBoundingBox();
		var rect2 = this.getBoundingBox();
		var bool = cc.rectIntersectsRect(rect,rect2);
		return bool;
	}



	


});