var GameLayer2 = cc.LayerColor.extend({
    numOfBall:0,
    ballStatus:2,
    max:5,
    min:0,
    x:null,
    hitFlag:0,
    num: 0,
    numOfSound: 0,




    ctor: function() {
        this._super( new cc.Color4B( 0, 0, 0, 0 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.maze = new Maze2(0,0);
        this.addChild( this.maze );


        //this.maze.scheduleUpdate();
        this.jumper = new Jumper( 1300, 150 ,this);
        this.x = cc.Follow.create( this.jumper, cc.rect( 0, 0, 1600, 1040) );
        this.runAction(this.x);

        this.scheduleUpdate();

        
        //this.jumper.setPositionZ(1);
        this.jumper.setBlocks( this.maze.blocks );
        this.addChild( this.jumper );
        this.scheduleOnce(function() {
            this.jumper.scheduleUpdate();
        }, 2);

        this.flagGoal = this.maze.getFlagGoal();
        

        this.maze.setPositionByBall(this.jumper);

        //this.maze.followBallAction();
        
        
        
        this.setKeyboardEnabled( true );
        
        return true;
    },

    changeBallStatus: function(e){
        var DetailLayer = this.getParent().getChildByTag("DetailLayer");

        if(e==81){
            if(this.ballStatus >this.min){
                 this.ballStatus--;
                 DetailLayer.updateText(this.ballStatus);
                 console.log('ballStatus' + this.ballStatus);
            }
        }
        else if(e==87){
            if(this.ballStatus <this.max){
                this.ballStatus++;
                DetailLayer.updateText(this.ballStatus);
                console.log('ballStatus' + this.ballStatus);
            }
        }
    },

    changeBall: function(){
        console.log('this.numOfBall'+this.numOfBall);
        if(this.numOfBall == 1){
            this.ball = new Ball1(this.jumper.getPositionX(),this.jumper.getPositionY());
            this.jumper.unscheduleUpdate();
            this.removeChild(this.jumper);
            
            //this.numOfBall = 0;

            this.jumper = this.ball;
            this.jumper.setBlocks( this.maze.blocks );
            this.addChild(this.jumper);
            this.jumper.scheduleUpdate();
            this.stopAllActions();

            this.x = cc.Follow.create( this.jumper, cc.rect( 0, 0, 1600, 1040) );
            this.runAction(this.x);
        }
        else if(this.numOfBall == 2){
            this.ball = new Jumper(this.jumper.getPositionX(),this.jumper.getPositionY());
            this.jumper.unscheduleUpdate();
            this.removeChild(this.jumper);
            
            //this.numOfBall = 0;

            this.jumper = this.ball;
            this.jumper.setBlocks( this.maze.blocks );
            this.addChild(this.jumper);
            this.jumper.scheduleUpdate();
            this.stopAllActions();

            this.x = cc.Follow.create( this.jumper, cc.rect( 0, 0, 1600, 1040) );
            this.runAction(this.x);

        }
        
        
    


    },

    update: function(){

    	if(!this.redMove || this.redMove[0].isDone() ) {
    		var redBlock = this.maze.getRedBlock();


    		this.redMove = [];
    		this.redMoveRight = [];
    		this.redMoveLeft = [];
    		for(var r =0; r < redBlock.length-1; r++){
    			this.redMoveRight[r] = cc.MoveTo.create(4,cc.p(1200-80*r,160));
    			this.redMoveLeft[r] = cc.MoveTo.create(4,cc.p(120+80*r,160));

    			this.redMove[r] = cc.Sequence.create(this.redMoveRight[r],this.redMoveLeft[r]);

    			redBlock[r].runAction(this.redMove[r]);
    		}

    		//redBlock[0].runAction(cc.MoveTo.create(2,cc.p(1200,160)));
    		//redBlock[1].runAction(cc.MoveTo.create(2,cc.p(1200,160)));
    		
    	}

    	if(this.jumper.getPositionX() >= 1480&&this.jumper.getPositionX() <= 1500){
    		if(this.jumper.getPositionY()==240){
    			this.num++;
    			if(this.num == 500){
    				var redBlock = this.maze.getRedBlock();
    				console.log('gogogogogo');
    				this.redGoFlag = cc.MoveTo.create(10,cc.p(960,400));
    				redBlock[3].runAction(this.redGoFlag);

    			}
    			
    		}
    	}

    	if(this.jumper.getPositionX() >= 1440&&this.jumper.getPositionX() <= 1520){
    			if(this.jumper.getPositionY()==372){
    				this.numOfSound++;
    				console.log(this.numOfSound);
    				var audioEngine = cc.AudioEngine.getInstance();
    				if(this.numOfSound %2== 1){
    					console.log('music');
            			audioEngine.playMusic(s_redSound, true);
    				}
    				else{
            			audioEngine.stopMusic(s_redSound);
    				}
    			}
    	}

    	console.log(this.jumper.getPositionX() +" " + this.jumper.getPositionY());


        
        if(this.flagGoal.toGoal(this.jumper)) {
        	this.hitFlag++;
            var DetailLayer = this.getParent().getChildByTag("DetailLayer");

    			if(this.hitFlag == 200){
    				DetailLayer.addText2();
    			}
            
            	else if(this.hitFlag == 400){
            		DetailLayer.removeText2();
            		var DetailLayer = this.getParent().getChildByTag("DetailLayer");
                	DetailLayer.updateClearStage();
            }
        }
                 
        //console.log(this.jumper.y);
    },

    Jumper: function() {
        return this.jumper; 
    },

    

    onKeyDown: function( e ) {
        console.log(e);
        if (e == cc.KEY.r)
            location.reload();
        this.jumper.handleKeyDown( e );
        
        this.changeBallStatus(e);
        if(e == 32){
            this.numOfBall = this.ballStatus;
            this.changeBall();
            
        }

        
        
    },

    onKeyUp: function( e ) {
        this.jumper.handleKeyUp( e );
        //this.ball.handleKeyUp(e);
    }
});

var StartScene2 = cc.Scene.extend({
    ctor: function() {
        this._super(); 

        this.addChild( new GameLayer2(),0,"GameLayer");
        var Game = this.getChildByTag("GameLayer");
        this.addChild( new DetailLayer(Game.ballStatus),100,"DetailLayer");
        this.addChild( new Background(),-100,"Background");
    }
});