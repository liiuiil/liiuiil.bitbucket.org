    var Jumper = cc.Sprite.extend({
    vx:0,
    vy:0,

    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'images/ball.png' );
        this.setAnchorPoint( cc.p( 0.5, 0 ) );
        this.x = x;
        this.y = y;

        this.maxVx = 8;
        this.accX = 0.25;
        this.backAccX = 0.5;
        this.jumpV = 20;
        this.g = -1;
        
        vx = 0;
        vy = 0;

        this.moveLeft = false;
        this.moveRight = false;
        this.jump = false;
        this.isHit = false;

        this.ground = null;

        this.blocks = [];

        this.updatePosition();
    },

    updatePosition: function() {
        this.setPosition( cc.p( Math.round( this.x ),
                                Math.round( this.y ) ) );
    },

    update: function() {
        var oldRect = this.getBoundingBoxToWorld();
        var oldX = this.x;
        var oldY = this.y;
        
        this.updateYMovement();
        this.updateXMovement();

        var dX = this.x - oldX;
        var dY = this.y - oldY;
        
        var newRect = cc.rect( oldRect.x + dX,
                               oldRect.y + dY - 1,
                               oldRect.width,
                               oldRect.height + 1 );

        this.handleCollision( oldRect, newRect );
        this.updatePosition();
        // if(this.moveRight)
        //     this.x += 5;
        // else if(this.moveLeft)
        //     this.x -= 5;
        // else if(this.jump)
        //     this.y += 5;
        // else if(this.down)
        //     this.y -= 5;
    },

    updateXMovement: function() {
        if ( this.ground ) {
            if ( ( !this.moveLeft ) && ( !this.moveRight ) ) {
                this.autoDeaccelerateX();
            } else if ( this.moveRight ) {
                if ( !this.isHit )
                    this.accelerateX( 1 );
            } else {
                this.accelerateX( -1 );
            }
        }
        this.x += this.vx;
        
    },

    updateYMovement: function() {
        if ( this.ground ) {
            this.vy = 0;
            if ( this.jump ) {
                this.jumpUp();
                }
        } else {
            this.fallDown();
        }      
    },
    jumpUp: function(){
        this.vy = this.jumpV;
        this.y = this.ground.getTopY() + this.vy;
        this.ground = null;
    },

    fallDown: function(){
        this.vy += this.g;
        this.y += this.vy;
    },

    isSameDirection: function( dir ) {
        return ( ( ( this.vx >=0 ) && ( dir >= 0 ) ) ||
                 ( ( this.vx <= 0 ) && ( dir <= 0 ) ) );
    },

    accelerateX: function( dir ) {
        if ( this.isSameDirection( dir ) ) {
            this.vx += dir * this.accX;
            if ( Math.abs( this.vx ) > this.maxVx ) {
                this.vx = dir * this.maxVx;
            }
        } else {
            if ( Math.abs( this.vx ) >= this.backAccX ) {
                this.vx += dir * this.backAccX;
            } else {
                this.vx = 0;
            }
        }
    },
    
    autoDeaccelerateX: function() {
        if ( Math.abs( this.vx ) < this.accX ) {
            this.vx = 0;
        } else if ( this.vx > 0 ) {
            this.vx -= this.accX;
        } else {
            this.vx += this.accX;
        }
    },

    handleCollision: function( oldRect, newRect ) {
        if ( this.ground ) {
            //console.log( this.ground.number+"XXX"+this.y);
            if ( !this.ground.onTop( newRect ) ) {
                this.ground = null;
            }
            if(this.vx >=0){
                var leftSideBlock1 = this.findLeftSideBlock(this.blocks, oldRect, newRect);
                if(leftSideBlock1){
                    this.vx = -this.vx/2;
                    if(cc.rectIntersectsRect(oldRect,leftSideBlock1)){
                        this.accelerateX( -5 );
                        console.log("Hit");
                        //this.isHit = true;
                                        }
                    //console.log("saffeffe");
                                    }
            }
            else if(this.vx <0){
                var rightSideBlock1 = this.findRightSideBlock(this.blocks, oldRect, newRect);
                if(rightSideBlock1){
                    this.vx = -this.vx/2;
                
                    if(cc.rectIntersectsRect(oldRect,rightSideBlock1)){
                        this.accelerateX( 5 );
                        
                                        }
                    //console.log("saffeffe");
                                    }
            }



        } else {
            if ( this.vy <= 0 ) {
                var topBlock = this.findTopBlock( this.blocks, oldRect, newRect );
                
                if ( topBlock ) {
                    this.notFallOnTopBox(topBlock);
                }
            }
            else if( this.vy >=0){
                var bottomBlock = this.findBottomBlock(this.blocks, oldRect, newRect );
                if(bottomBlock){
                    this.vy=0;
                }
            }
            if(this.vx >=0){
               var leftSideBlock = this.findLeftSideBlock(this.blocks, oldRect, newRect);
               if(leftSideBlock){
                    if(cc.rectIntersectsRect(oldRect,leftSideBlock)){
                        this.vx = -this.vx/5;
                        this.accelerateX(-5);
                    }

                }
            }
            else if(this.vx<0){
                //console.log('right');
                var rightSideBlock = this.findRightSideBlock(this.blocks, oldRect, newRect);
               if(rightSideBlock){
                    if(cc.rectIntersectsRect(oldRect,rightSideBlock)){
                        this.vx = -this.vx/5;
                        this.accelerateX(5);
                    }

                }
            }
        }
        //this.findBlockInSameY(this.blocks,this.getBoundingBox());
    },

    notFallOnTopBox: function(topBlock){
        this.ground = topBlock;
        //console.log(this.ground.number+"#222//"+this.ground.getTopY()+"%"+this.y);
        this.y = this.ground.getTopY();
        this.vy = 0;

    },
    
    findTopBlock: function( blocks, oldRect, newRect ) {
        var topBlock = null;
        var topBlockY = -1;
        
        blocks.forEach( function( b ) {
            if ( b.hitTop( oldRect, newRect ) ) {
                //console.log(b.number+"Find");
                if ( b.getTopY() > topBlockY ) {
                    topBlockY = b.getTopY();
                    topBlock = b;
                }
            }
        }, this );
        
        return topBlock;
    },
    findBottomBlock: function( blocks, oldRect, newRect ) {
        var bottomBlock = null;
        var bottomBlockY = 1000;
        
        blocks.forEach( function( b ) {
            if ( b.hitBottom( oldRect, newRect ) ) {
                if ( b.getBottomY() < bottomBlockY ) {
                    bottomBlockY = b.getTopY();
                    bottomBlock = b;
                }
            }
        }, this );
        
        return bottomBlock;
    },

    findLeftSideBlock: function( blocks, oldRect, newRect ) {
        var leftSideBlock = null;
        var leftSideBlockX = 1000;
        
        blocks.forEach( function( b ) {
            if ( b.hitLeftSide( oldRect, newRect ) ) {
                if ( b.getLeftX() < leftSideBlockX) {
                    leftSideBlockX = b.getLeftX();
                    leftSideBlock = b;
                }
            }
        }, this );
        
        return leftSideBlock;
    },

    findRightSideBlock: function( blocks, oldRect, newRect ) {
        var rightSideBlock = null;
        var rightSideBlockX = -1;
        
        blocks.forEach( function( b ) {
            if ( b.hitRightSide( oldRect, newRect ) ) {
                if ( b.getRightX() > rightSideBlockX) {
                    rightSideBlockX = b.getRightX();
                    rightSideBlock = b;
                }
            }
        }, this );
        
        return rightSideBlock;
    },
    // findBlockInSameY: function( blocks, rect ){
    //     //console.log(rect.x+"#"+(rect.x-10)+"_"+rect.y+"#"+(rect.y+10)+"$"+cc.rectGetMaxX(rect)+"%"+cc.rectGetMinX(rect));
    //     var newRectLeft = cc.rect( rect.x-10, rect.y+10, 5, 5);
    //     console.log(cc.rectGetMinY(newRectLeft)+"%"+cc.rectGetMaxY(newRectLeft));
    //     var newRectRight = cc.rect( rect.x+10, rect.y+10, 5, 5);
    //     console.log(cc.rectGetMinY(newRectRight)+"%"+cc.rectGetMaxY(newRectRight)+"#"+rect.x);
    //     blocks.forEach( function( b ){
    //         if(cc.rectIntersectsRect( b, newRectLeft) || cc.rectIntersectsRect( b, newRectRight )){
    //             //this.vx = 0;
    //             console.log("True");
    //         }else{
    //             console.log("False");
    //         }
    //     }, this); 
    // },

    // findLeftSideBlock11: function( blocks, oldRect, newRect ) {
    //     var leftSideBlock = null;
    //     var leftSideBlockX = 1000;
        
    //     blocks.forEach( function( b ) {
    //         //console.log(cc.rectGetMinY(b.getBoundingBoxToWorld()));
    //         if ( cc.rectGetMinY(b.getBoundingBoxToWorld()) == cc.rectGetMinY(this.getBoundingBoxToWorld() ) ) {
    //             console.log("HERE");
    //             if(Math.abs(b.getPosition().x - this.getPosition().x) <= 800 ) {
    //             console.log(b.getPosition().x + "...." + this.getPosition().x);
    //             console.log("KUY");
    //             leftSideBlockX = b.getLeftX();
    //             leftSideBlock = b;
    //             }
    //             if ( b.getLeftX() < leftSideBlockX) {
    //                 leftSideBlockX = b.getLeftX();
    //                 leftSideBlock = b;
    //             }
    //         }
    //     }, this );
        
    //     return leftSideBlock;
    // },

    
    handleKeyDown: function( e ) {
        if ( Jumper.KEYMAP[ e ] != undefined ) {
            this[ Jumper.KEYMAP[ e ] ] = true;
            if ( e != cc.KEY.right )
                this.isHit = false;
        }
    },

    handleKeyUp: function( e ) {
        if ( Jumper.KEYMAP[ e ] != undefined ) {
            this[ Jumper.KEYMAP[ e ] ] = false;
        }
    },

    setBlocks: function( blocks ) {
        this.blocks = blocks;
    }
});

Jumper.KEYMAP = {}
Jumper.KEYMAP[cc.KEY.left] = 'moveLeft';
Jumper.KEYMAP[cc.KEY.right] = 'moveRight';
Jumper.KEYMAP[cc.KEY.up] = 'jump';
Jumper.KEYMAP[cc.KEY.down] = 'down';