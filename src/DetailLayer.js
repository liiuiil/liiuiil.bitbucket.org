var DetailLayer = cc.LayerColor.extend({
	ctor: function(ballStatus){
		this._super(new cc.Color4B( 0, 0, 0, 0 ) );

		var director = cc.Director.getInstance();
		var winSize = director.getWinSize();
		var centerPos = cc.p(winSize.width/2, winSize.height/2);
		this.myLabel = cc.LabelTTF.create('NormalBall',  'Times New Roman', 32);
		this.myLabel.setPosition(100,550);
		this.addChild(this.myLabel);

		this.setPosition(0,0);

		// this.followBall = cc.Follow.create( this.newJumper, cc.rect( 0, 0, 600, 800) );
  //       this.runAction(this.followBall);

        this.scheduleUpdate();
        return true;

	},
	
	updateText: function( ballStatus ){
		if(ballStatus == 1){
			this.myLabel.setString( 'BigBall' );
		}
		else if(ballStatus ==2 ){
			this.myLabel.setString( 'NormalBall' );
		}
		else{
			this.myLabel.setString( 'None' );
		}
	},

	updateClearStage: function(){
		this.stageClear = new StageClear();
		this.stageClear.setPosition( cc.p( 400, 300 ) );
		this.setOpacity(0);
        this.addChild(this.stageClear);
        this.stageClear.runAction(cc.FadeIn.create(0.4));

	},

	addText2: function(){
		this.myLabel2 = cc.LabelTTF.create('...?',  'Times New Roman', 100);
		this.myLabel2.setPosition(395,300);
		this.addChild(this.myLabel2);
	},

	removeText2: function(){
		this.removeChild(this.myLabel2);
	},

	addText3: function(){
		this.myLabel3 = cc.LabelTTF.create('Do you know? The ball can pass the blocks',  'Times New Roman', 40);
		this.myLabel3.setPosition(350,50);
		this.addChild(this.myLabel3);

	},

	removeText3: function(){
		this.removeChild(this.myLabel3);
	}



});